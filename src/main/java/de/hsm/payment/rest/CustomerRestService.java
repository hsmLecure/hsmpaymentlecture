/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsm.payment.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hsm.payment.entity.Customer;
import de.hsm.payment.services.CustomerManager;
import de.hsm.payment.services.CustomerService;
import java.io.IOException;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONObject;

/**
 * REST Web Service
 *
 * @author effertz
 */
@Path("/")
public class CustomerRestService {

    @Context
    private UriInfo context;
    
    private CustomerService customerService;
    private ObjectMapper objectMapper ;

    /**
     * Creates a new instance of CustomerRestService
     */
    public CustomerRestService() {
        if (customerService == null){
            customerService = new CustomerManager();
        }
        objectMapper = new ObjectMapper();
    }

    /**
     * Retrieves the representation of a @Customer instance.
     *
     * @param customerId the path parameter
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/customer/{id}")
    public String getCustomer(@PathParam("id") int customerId) {

        String json = "{\"id\": \" " + customerId + "\"}";
        return json;
    }
    
    /**
     * Retrieves the representation of a @Customer instance.
     *
     * @param queryParam the query parameter
     * @return an instance of java.lang.String
     */
    @GET
    @Path("/customer")
    public Response getCustomerQueryVariable(@Context HttpHeaders headers, 
            @QueryParam("id") int id) throws JsonProcessingException {
        
        Customer customer = customerService.getCustomer(id);
        String resultJson = objectMapper.writeValueAsString(customer);

        Response response = Response.status(Response.Status.OK).
                header("ourHeader", "ourValue").entity(resultJson).build();
        
        return response;
    }
    
    @GET
    @Path("/customers")
    public Response getCustomers() throws JsonProcessingException{
    
        List<Customer> customerList = customerService.getCustomerList();
        String customerListJson = objectMapper.writeValueAsString(customerList);
        
        return Response.ok(customerListJson).build();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/customer")
    public Response createCustomer(String customerJson) throws IOException {

        Customer customer = objectMapper.readValue(customerJson, Customer.class);
        
        customerService.createCustomer(customer.getFirstName(), 
                customer.getLastName(), customer.geteMailAddress());
        
        String customerJson2 = objectMapper.writeValueAsString(customer);

        Response response = Response.status(Response.Status.OK).
                entity(customerJson2).build();
        
        return response;
    }
    
    /**
     * PUT method for updating or creating an @Customer instance
     *
     * @param json representation for the resource to update
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public String updateCustomer(String json) {

        String s = "PUT response: " + json;
        return s;
    }
}
