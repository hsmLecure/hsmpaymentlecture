/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsm.payment.services;

import de.hsm.payment.entity.Customer;
import de.hsm.payment.entity.PaymentAccount;
import de.hsm.payment.entity.Phone;
import java.util.Calendar;

/**
 *
 * @author effertz
 */
public interface AccountService {
    
     PaymentAccount save(PaymentAccount account);
     
     PaymentAccount save(String iban, Customer customer);
     
     PaymentAccount save(String iban, String firstName, String lastName, 
             Calendar brithDate, Phone phone, String email);
     
     PaymentAccount get(String iban);
     
     PaymentAccount get(String firstName, String lastName);
     
     boolean delete (String iban);
}
