/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsm.payment.services;

import de.hsm.payment.entity.Customer;
import de.hsm.payment.entity.Phone;
import java.util.List;

/**
 *
 * @author effertz
 */
public interface CustomerService {

    Customer getCustomer(String name);
    
    List<Customer> getCustomerList();
    
    Customer getCustomer(int id);
    
    Customer createCustomer(Customer customer );

    Customer createCustomer(String fristName, String lastName, String emailAddress);
    
    Customer updateCustomer(Customer customer );
    
    Customer updateCustomer(String fristName, String lastName, String emailAddress);
    
    Phone updatePhone(Phone phone );
    
    boolean deleteCustomer(int id);
}
