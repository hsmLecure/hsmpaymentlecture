/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsm.payment.services;

import de.hsm.payment.entity.Customer;
import de.hsm.payment.entity.Phone;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author effertz
 */
public class CustomerManager implements CustomerService{
    
    private List<Customer> customers;
    
    @Override
    public Customer createCustomer(Customer customer) {
  
        if (customers == null) {
            customers = new ArrayList();
        }
        customer.seteMailAddress(customer.getFirstName()+"."+customer.getLastName()+"@hsm.de");
        customers.add(customer);
        return customer;
    }
    
    @Override
    public Customer createCustomer(String fristName, String lastName, String emailAddress) {
        
        Customer c = new Customer();
        c.setFirstName(fristName);
        c.setLastName(lastName);
        c.seteMailAddress(emailAddress);
        
        return this.createCustomer(c);
    }

    @Override
    public Customer getCustomer(String name) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Customer updateCustomer(Customer customer) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Customer updateCustomer(String fristName, String lastName, String emailAddress) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Phone updatePhone(Phone phone) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean deleteCustomer(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Customer getCustomer(int id) {
        
        Customer customer = new Customer();
        customer.setId(id);
        customer.setFirstName("John");
        customer.setLastName("Doe");
        customer.seteMailAddress("john.doe@mail.com");
        
        customer.setPhoneNumber(new Phone(49, 36383, 12345678));
        
        
        return customer;  
    }

    @Override
    public List<Customer> getCustomerList() {
        return customers;
    }
}
