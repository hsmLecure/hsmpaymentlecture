/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsm.payment.services;

import de.hsm.payment.entity.CurrencyCode;
import de.hsm.payment.entity.PaymentAccount;
import de.hsm.payment.entity.PaymentTransaction;
import java.util.List;

/**
 *
 * @author effertz
 */
public interface PaymentService {
    
   
    PaymentAccount createPaymentAccount(PaymentAccount paymentAccount);
    
    boolean deletePaymentAccount(String iban);
    
    double checkAccountBalance(String iban);
    
    List<PaymentTransaction> getTransactions();
    
    boolean transferMoney(String senderIban, String receiverIban, double amount, 
            CurrencyCode currency);
    
    
}
